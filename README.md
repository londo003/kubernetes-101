Kubernetes 101
---

[DHE K8s](https://ocpconsole.duhs.duke.edu:8443/console)

[Oasis Deployment Utility Image Project](https://gitlab.dhe.duke.edu/ori-rad/ci-pipeline-utilities/deployment)

### Initial Setup
1. Create a file called .personal.namespace in your home directory
```
PROJECT_NAMESPACE=yournamespace
CLUSTER_SERVER=https://ocpconsole.duhs.duke.edu:8443
HELM_USER=helm-deployer
HELM_TOKEN=yourtoken
CHART_MUSEUM_URL=https://museum.ocp.dhe.duke.edu
```

2. Use the [Oasis Deployment Utility Image 1.3.6](gitlab.dhe.duke.edu:4567/ori-rad/ci-pipeline-utilities/deployment:1.3.6)
```
docker run -ti --rm -v`pwd`:/workdir --workdir /workdir --env-file ~/.personal.namespace  gitlab.dhe.duke.edu:4567/ori-rad/ci-pipeline-utilities/deployment:1.3.6 bash
```

3. source the helper libraries
```
export LOG_LEVEL=info
source /usr/local/lib/helpers/common
source /usr/local/lib/helpers/helm
```

4. Login to the Cluster with your HELM_USER and set the context to your PROJECT_NAMESPACE
```
cluster_login
```

5. Play with kubectl
```
kubectl get all
kubectl get deployments
kubectl get services
```

### Deployments

[Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)

1. create a deployment with an existing hello-world image
```
kubectl create deployment ${netid}-hello-k8s --image=paulbouwer/hello-kubernetes:1.8
```

2. watch until its pod is Running, print its log
```
kubectl get pods
kubectl logs deployment/${netid}-hello-k8s
```

3. shell into the running pod (your pod name will be different)
```
kubectl exec -ti ${netid}-hello-k8s-5f4dbc7fcc-lnrgc sh
/usr/src/app $ wget -O- localhost:8080
...
/usr/src/app $ exit
```

4. remove deployment
```
kubectl delete deployment/${netid}-hello-k8s
kubectl get deployments
kubectl get pods
```

5. Create a app.yaml file using dry-run and output from kubectl
```
kubectl create deployment ${netid}-hello-k8s --image=paulbouwer/hello-kubernetes:1.8 --dry-run -o yaml > app.yaml
```

6. clean it up, add named ports, and deploy
```
kubectl apply -f app.yaml
```

### Services

[Service](https://kubernetes.io/docs/concepts/services-networking/service/)

1. create a clusterip service using the hello-k8s selector
```
 kubectl create service clusterip ${netid}-hello-k8s --tcp=8080:8080
```

2. test the service
```
kubectl get pods -l app=${netid}-hello-k8s -o name
kubectl exec -ti $podname sh
/usr/src/app $ wget -O- ${netid}-hello-k8s:8080
...
/usr/src/app $ exit
```

3. remove service, use dry-run -o yaml to append to app.yaml
```
echo '---' >> app.yaml
kubectl create service clusterip ${netid}-hello-k8s --tcp=8080:8080 --dry-run -o yaml >> app.yaml
```

4. edit for named ports, deploy
```
kubectl apply -f app.yaml
```

### Routes (Ingress)

Routes are Openshift specific objects. They will not work in other kubernetes distributions, such as Rancher. Kubernetes uses Ingress objects to do what Routes do in Openshift. Eventually we will move away from Openshift and will have to learn about Ingress. Right now we just need to understand routes.

1. Use the openshift webconsole to create a Route from the ${netid}-hello-k8s service.
You must click 'secure route', choose TLS Termination 'Edge', Insecure Traffic: 'Redirect'. It will generate a real cert for you, and a default host url. Alternatively, you could install `oc` on your workstation, and use it to create a route, but this tutorial will not cover `oc`.

2. export that route to your app.yaml
```
echo '---' >> app.yaml
kubectl get route/${netid}-hello-k8s -o yaml --export >> app.yaml
```

3. delete the route, edit to add status.ingress and apply app.yaml
```
kubectl delete route/${netid}-hello-k8s
kubectl apply -f app.yaml
```

4. Cleanup
```
kubectl delete all -l app=${netid}-hello-k8s
```

### Additional Deployments

1. copy app.yaml to app2.yaml.

2. Edit to replace ${netid}-hello-k8s with ${netid}-hello-k8s-2

3. deploy app.yaml and app2.yaml at the same time

4. visit both urls, note the difference in the pod name

### Pods

[Pod](https://kubernetes.io/docs/concepts/workloads/pods/)

Deployments actually define a number of Kubernetes Objects in addition to the Deployment
object. The most important Objects are Pods. The Deployment.spec.template defines the
attributes that go into the Pod objects that get created (based on the number of replicas).
This same template is used in other Kubernetes objects that describe Pods, e.g. 
Statefulsets, Jobs, and even Pods themselves. It is a good idea to read about Pods, and
their attributes.

- image
- command
- environment
- ImagePullSecrets
- Liveness/Readiness Probes

### Helm Preview
[Helm](https://helm.sh/)

you can create a boilerplate helm (version 3) chart using the deployment image. Once inside, with your workdir volume mounted to your host, you can
run helm create $path and it will create a directory at the $path with helm
boilerplate. I usually create a helm-chart directory in my application roots,
and create the helm-chart in that directory, named for the project.
```
mkdir helm-chart
helm create helm-chart/hello-k8s
```

You can look at the helm-chart/hello-k8s/templates directory for a deployment,
service, etc. You would need to replace ingress.yaml with route.yaml and make
a route definition.